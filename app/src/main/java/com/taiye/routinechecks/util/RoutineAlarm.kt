package com.taiye.routinechecks.util

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import android.support.v4.content.ContextCompat.getSystemService
import android.widget.Toast
import com.taiye.routinechecks.MainApplication




class RoutineAlarm: BroadcastReceiver(){

    val context = MainApplication.appContext
    val thirtyMinutes = SystemClock.elapsedRealtime() + 30 * 1000



    companion object {
       val ACTION_ALARM = "com.taiye.routinechecks.ACTION_ALARM"
   }

    override fun onReceive(context: Context?, intent: Intent?) {
        if (ACTION_ALARM.equals(intent?.getAction())) {
            Toast.makeText(context, ACTION_ALARM, Toast.LENGTH_SHORT).show()
        }
    }




     fun setAlarm(time: Long) {
        val intentToFire = Intent(context, RoutineAlarm::class.java)
        intentToFire.action = RoutineAlarm.ACTION_ALARM
        val alarmIntent = PendingIntent.getBroadcast(context, 0, intentToFire, 0)
        val alarmManager = getSystemService(context,RoutineAlarm::class.java) as AlarmManager?
        alarmManager!!.set(AlarmManager.ELAPSED_REALTIME, time, alarmIntent)
    }



}