package com.taiye.routinechecks.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "routine")
data class Routine(@PrimaryKey(autoGenerate = true) val id: Int?, var title:String, var description:String)
{
    constructor():this(null, "title","description")
}