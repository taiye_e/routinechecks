package com.taiye.routinechecks.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.taiye.routinechecks.R
import com.taiye.routinechecks.model.Routine
import com.taiye.routinechecks.viewmodel.RoutineViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver


class RoutineActivity : AppCompatActivity() {

    lateinit var routineViewModel: RoutineViewModel
    private lateinit var  routineFrequencySpinner:Spinner
    private val disposable = CompositeDisposable()
    private var mNewRoutine: Boolean = false
    private lateinit var etTitle:EditText
    private lateinit var etDesc:EditText
    private lateinit var save:Button




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_routine)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_check)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)



        etTitle = findViewById(R.id.etTitle)
        etDesc = findViewById(R.id.etDesc)


        initViewModel()
        setUpSpinnerConfigurable()

        save = findViewById(R.id.save_routine)

        save.setOnClickListener { view ->
            saveRoutine()
        }

    }

    private fun initViewModel() {

    }

    private fun setUpSpinnerConfigurable() {
        routineFrequencySpinner = findViewById(R.id.routineSpinner) as Spinner
        val frequency = arrayOf("Daily", "Weekly", "Monthly")
        val colorAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, frequency)
        colorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        routineFrequencySpinner.setAdapter(colorAdapter)
        routineFrequencySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                setUpConfigurableOnClickSpinner()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

    }

    private fun setUpConfigurableOnClickSpinner() {
        val frequency = routineFrequencySpinner.getSelectedItem().toString()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (mNewRoutine) {
            val inflater = menuInflater
            inflater.inflate(R.menu.menu_routine, menu)
        }
          return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            return true
        } else if (item.itemId == R.id.action_delete) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }



    fun saveRoutine() {
        routineViewModel = ViewModelProviders.of(this).get(RoutineViewModel::class.java)
        val routine = Routine(0,etDesc.text.toString(),etTitle.text.toString())
        routineViewModel.createRoutine(routine)
                   .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : DisposableSingleObserver<Routine>() {
                    override fun onSuccess(value: Routine?) {
                        Toast.makeText(applicationContext, "Routine saved successfully", Toast.LENGTH_LONG).show()
                    }
                    override fun onError(e: Throwable?) {
                        Toast.makeText(applicationContext, "An error occured, please try again", Toast.LENGTH_LONG).show()
                    }
                })
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }

}