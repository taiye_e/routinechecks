package com.taiye.routinechecks.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.taiye.routinechecks.model.Routine

@Database(entities = [Routine::class], version = 1)
@TypeConverters(DateTypeConverter::class)
abstract class AppDatabase: RoomDatabase() {

    companion object {
        var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "routine_checks").build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase() {
            INSTANCE = null
        }

    }

    abstract fun routineDao(): RoutineDao
}