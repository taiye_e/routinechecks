package com.taiye.routinechecks.database

import android.content.Context
import com.taiye.routinechecks.model.Routine
import io.reactivex.Flowable
import io.reactivex.Single

class AppRepository(var  context: Context) {

    private lateinit   var  instance: AppRepository
    private lateinit var routines: Flowable<List<Routine>>
    private  lateinit var database: AppDatabase

    init {
       database = AppDatabase.getInstance(context)!!
        routines = getAllRoutines()
    }

    companion object {
         var instance: AppRepository? = null
        fun getInstance(context: Context): AppRepository? {
            if (instance == null) {
                synchronized(AppRepository::class) {
                    instance = AppRepository(context)
                }
            }
            return instance
        }
    }



    fun getAllRoutines(): Flowable<List<Routine>> {
        return  database.routineDao().getAll()
    }


    fun createRoutine(routine:Routine?):Single<Routine>{
      return database.routineDao().createNote(routine)
    }


}