package com.taiye.routinechecks.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.taiye.routinechecks.model.Routine
import io.reactivex.Flowable
import io.reactivex.Single


@Dao
interface RoutineDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRoutine(routine: Routine)

    @Query("SELECT * FROM routine")
    fun getAll(): Flowable<List<Routine>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createNote(routine:Routine?): Single<Routine>
}