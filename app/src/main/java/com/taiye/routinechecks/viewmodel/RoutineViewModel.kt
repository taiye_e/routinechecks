package com.taiye.routinechecks.viewmodel

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.widget.Toast
import com.taiye.routinechecks.MainApplication
import com.taiye.routinechecks.database.AppRepository
import com.taiye.routinechecks.model.Routine
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class RoutineViewModel : ViewModel() {
    val context = MainApplication.appContext
    private val disposable = CompositeDisposable()
    private lateinit var mRepository: AppRepository
    lateinit var routine: Single<Routine>


    init {
        mRepository = AppRepository(context.applicationContext)
    }


    fun RoutineViewModel(application: Application) {
        mRepository = AppRepository.getInstance(application)!!
    }


    fun createRoutine(routine: Routine?): Single<Routine> {
        return mRepository.createRoutine(routine)
    }


    fun saveRoutine(routine: Routine?) {
        disposable.add(mRepository.createRoutine(routine)
            .subscribeOn(Schedulers.io())
             .observeOn(AndroidSchedulers.mainThread())
              .subscribeWith(object : DisposableSingleObserver<Routine>() {
                  override fun onSuccess(value: Routine?) {
                                Toast.makeText(context, "Routine saved successfully", Toast.LENGTH_LONG).show()
               }
                  override fun onError(e: Throwable?) {
                                Toast.makeText(context, "An error occured, please try again", Toast.LENGTH_LONG).show()
                            }
              }))
    }

}
